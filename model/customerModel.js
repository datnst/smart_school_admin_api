﻿const mongoose = require('mongoose');



const customerSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    customerNumber: Number,
    customerName: String,
    contactLastName: String,
    contactFirstName: String,
    phone: String,
    addressLine1: String,
    addressLine2: String,
    city: String,
    state: String,
    postalCode: Number,
    country: String,
    salesRepEmployeeNumber: Number,
    creditLimit: Number,
    customerPhoto: String


})

module.exports = mongoose.model('Customer', customerSchema);

