﻿const mongoose = require('mongoose');

const studentSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    idCardNumber: Number,
    phoneNumber: String,
    gmail: String,
    userName: String,
    password: String
})

module.exports = mongoose.model('Student', studentSchema);