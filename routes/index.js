﻿'use strict';
var express = require('express');
var router = express.Router();
var Student = require('../model/studentModel');
var Customer = require('../model/customerModel');
var mongoose = require('mongoose');
const customerController = require('../controller/customer.controller')

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Express' });
});

//router.post("/student", async (request, response) => {
//    try {
//        const student = new Student({
//            _id : new mongoose.Types.ObjectId,
//            userName : request.body.userName,
//            password : request.body.password
//        });
//        var result = await student.save();
//        response.status(200).send({
//            message: 'student was created',
//            result: result
//        });
//    } catch (error) {
//        response.status(500).send(error);
//    }
//});

//router.get("/customers", async (request, response) => {

//    try {
//        var result = await customer.find();
//        response.status(200).send({
//            result: result
//        });
//    } catch (error) {
//        response.status(500).send(error);
//    }
//});

router.get("/customers", customerController.customers);
router.get("/customer", customerController.customer);
router.post("/customer", customerController.saveOrUpdate);

router.get("/student", async (request, response) => { });
router.get("/student/:id", async (request, response) => { });
router.put("/student/:id", async (request, response) => { });
router.delete("/student/:id", async (request, response) => { });

module.exports = router;
