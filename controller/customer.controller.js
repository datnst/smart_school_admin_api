﻿'use strict';
var Customer = require('../model/customerModel');
const mongoose = require('mongoose');
var customerController = {
    customers : async (req, res) => {
        try {
            var result = await Customer.find();
            console.log(result);
            res.status(200).send({
                data: result
            });
        } catch (error) {
            res.status(500).send(error);
        }
    },
    customer: async (req, res) => {
        console.log(`id : ${req.query.id}`);
        let _id = req.query.id;
        try {
            var result = await Customer.findById(_id);
            console.log(result);
            res.status(200).send({
                data: result
            });
        } catch (error) {
            res.status(500).send(error);
        }
    },

    saveOrUpdate: async (req, res) => {
     
        var customer = {};
        
        try {
            if (req.body._id === "" || req.body._id === undefined) {
                customer = new Customer({
                    _id: new mongoose.Types.ObjectId(),
                    customerNumber: req.body.customerNumber,
                    contactFirstName: req.body.contactFirstName,
                    contactLastName: req.body.contactLastName,
                    phone: req.body.phone,
                    addressLine1: req.body.addressLine1,
                    addressLine2: req.body.addressLine2
                }) 

                var result = await Customer.create(customer);
                console.log(result);
                res.redirect(`http://localhost:3000/customer?id=${customer._id}&viewMode=VIEW`);
               
            } else {
                customer = {
                    customerNumber: req.body.customerNumber,
                    contactFirstName: req.body.contactFirstName,
                    contactLastName: req.body.contactLastName,
                    phone: req.body.phone,
                    addressLine1: req.body.addressLine1,
                    addressLine2: req.body.addressLine2
                }
             
                await Customer.updateOne({ _id: req.body._id }, customer);
                res.redirect(`http://localhost:3000/customer?id=${req.body._id}&viewMode=VIEW`);
            }

        } catch (error) {
            console.log(error);
            res.status(500).send(error);
        }
    },

    delete: async (req, res) => {
        try {
            if (req.body._id === "" || req.body._id === undefined) {
                var result = await Customer.remove({ _id :req.body._id });
                console.log(result);
                res.redirect(`http://localhost:3000/customers`);
            } 

        } catch (error) {
            console.log(error);
            res.status(500).send(error);
        }
    }
}

module.exports = customerController;